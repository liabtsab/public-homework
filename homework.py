import yaml

def _finditem(obj, key):
    if key in obj:
        return obj[key]
    for k,v in obj.items():
        if isinstance(v,dict):
            item = _finditem(v, key)
            if item is not None:
                return item


def flatten_list(_2d_list):
    flat_list = []
    # Iterate through the outer list
    for element in _2d_list:
        if type(element) is list:
            # If the element is of type list, iterate through the sublist
            for item in element:
                flat_list.append(item)
        else:
            flat_list.append(element)
    return flat_list


def get_member_data(mem_list,user_list):
    data_list = list()
    for member in mem_list:
        for user in user_list:
            if user['name'] == member:
                gl_handle = user['gitlab']
                break
            else:
                gl_handle = 'N/A'

        user_data_dict = {
            'member_name': member,
            'member_handle': gl_handle
        }
        data_list.append(user_data_dict)
    return data_list


# load file containing group members
with open('c://users//lia.cha//desktop//stages.yml') as s:
    stages = yaml.load(s, Loader=yaml.FullLoader)

# load file containing member gitlab metadata
with open('c://users//lia.cha//desktop//team.yml', encoding='utf-8') as u:
    users = yaml.load(u, Loader=yaml.FullLoader)

# load file containing group names
with open('c://users//lia.cha//desktop//groups.txt') as g:
    groups = [line for line in g.read().splitlines()]

check_keys = [
    'pm',
    'pmm',
    'cm',
    'pdm',
    'ux',
    'uxr',
    'sets',
    'support',
    'tech_writer',
    'tw_backup',
    'appsec_engineer',
    'backend_engineering_manager',
    'frontend_engineering_manager'
]

group_member_list = list()

for group in groups:
    group_and_members_dict = dict()
    user_data_list = list()
    manager_data_list = list()
    members_list = list()
    managers_list = list()
    group_and_members_dict['group_name'] = group
    stage_name = group.split('-')[2]
    group_name = group.split('-')[-1]
    stage_name_key = _finditem(stages,stage_name)
    if stage_name_key is not None:
        group_name_key = _finditem(stages,group_name)
        if group_name_key is not None:
            for k,v in group_name_key.items():
                if k in check_keys:
                    if 'manager' in k:
                        managers_list.append(v)
                    else:
                        members_list.append(v)
            consolidated_members_list = flatten_list(members_list)
            consolidated_managers_list = flatten_list(managers_list) # just adding to support cases where there may be multiple managers

            # build dict of members and gitlab handles
            member_data = get_member_data(consolidated_members_list,users)
            manager_data = get_member_data(consolidated_managers_list,users)
            
            # add member dict to each list
            user_data_list.append(member_data)
            manager_data_list.append(manager_data)

            # nest member and manager lists under parent dict and list
            group_and_members_dict['group_members'] = user_data_list
            group_and_members_dict['group_managers'] = manager_data_list
            group_member_list.append(group_and_members_dict)
    else:
        print('could not find data for the stage: {}'.format(stage_name))